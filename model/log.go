package model

import (
	"fmt"
	"github.com/gogf/gf/os/gfile"
	"github.com/gogf/gf/os/gtime"
)

var LOG_PATH = "ddns.log"

//自定义日志输出
func LogText(text string) {
	s := gtime.Datetime() + " " + text
	fmt.Println(s)
	if !gfile.Exists(LOG_PATH) {
		gfile.Create(LOG_PATH)
	}
	gfile.PutContentsAppend(LOG_PATH, s+"\n")
}
