package main

import (
	"HuaweiDDNS/model"
	"fmt"
	"github.com/gogf/gf/frame/g"
	"os"
	"time"
)

var config = g.Cfg().ToMap()

func main() {

	userName := checkParam("IAM用户所属帐号名", "userName")
	iamName := checkParam("IAM用户名", "iamName")
	iamPaasswd := checkParam("IAM用户密码", "iamPaasswd")

	domainHost := checkParam("域名主机", "domainHost")
	domainName := checkParam("域名地址", "domainName")

	getIpUrl := checkParam("获取ip地址", "getIpUrl")
	iamApiUrl := checkParam("iam地址", "iamApiUrl")
	dnsApiUrl := checkParam("dns地址", "dnsApiUrl")

	modelDdns := model.ModelDdns{userName, iamName, iamPaasswd, domainHost, domainName, getIpUrl, iamApiUrl, dnsApiUrl}

	exeTime := 300
	t, ok := config["time"]
	if ok {
		exeTime = int(t.(float64))
	}
	model.LogText(fmt.Sprintf("开始执行，时间间隔为%d秒", exeTime))
	for true {
		modelDdns.Run()
		//睡眠
		time.Sleep(time.Second * time.Duration(exeTime))
	}
	model.LogText("程序已退出")
}

func checkParam(name string, key string) string {
	userName, ok := config[key]
	if !ok {
		fmt.Printf("参数不能为空:%s\n", name)
		os.Exit(0)
	}
	return userName.(string)
}
