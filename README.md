### 华为云DDNS解析服务

### 使用说明
#### 1.华为云配置

##### 在华为云控制台【DNS】添加域名-公网解析-A记录
```
https://console.huaweicloud.com/dns
```
#### 2.修改配置

下载软件(pkd/HuaweiDDNS)和配置文件(config.toml)，修改相应华为云账号信息

#### 3.为程序添加可执行权限,运行
```
chmod 755 HuaweiDDNS

nohup ./HuaweiDDNS > log 2>&1 
```

#### 4.常见问题
（1）IAM账号

IAM账号一般就是华为云账号，除非是权限需要特殊新建的，可在控制台-IAM:查询

（2）IAM地址

根据区域填写,不清楚请[点此查看](https://developer.huaweicloud.com/endpoint?IAM)
例如华南广州:
- iam.cn-south-1.myhuaweicloud.com
- dns.cn-south-1.myhuaweicloud.com

（3）服务器不能解析域名

```
##测试
ping www.baidu.com
```
dns的问题，需要配置dns服务器